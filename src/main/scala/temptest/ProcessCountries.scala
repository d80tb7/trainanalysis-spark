package temptest

import trainanalysis.spark.BaseSparkJob
import temptest.data.Country
import org.apache.spark.sql.SaveMode.Overwrite

object ProcessCountries extends BaseSparkJob {

  def appName = "Process Country Files"

  def main(args: Array[String]) {
    execute((sparkContext, sqlContext) => {

      import sqlContext.implicits._

      val inputFiles = "/Users/chris/temperature/data/countries.txt"
      val outputFile = "/Users/chris/temparature/transformed/countries.parquet"

      val lines = sparkContext.textFile(inputFiles)
      val stations = lines.map(Country.fromCsv)
      stations.toDF.write.mode(Overwrite).parquet(outputFile)
    })
  }

}