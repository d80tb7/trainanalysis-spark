package temptest

import org.apache.spark.sql.SaveMode.Overwrite
import org.apache.spark.sql.functions._
import java.time.LocalDate
import java.sql.Date
import scala.reflect.runtime.universe
import trainanalysis.spark.BaseSparkJob
import temptest.data.Reading
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.hdfs.DFSClient.Conf
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path

object ProcessWeather extends BaseSparkJob {

  def appName = "Process Weather Files"

  def main(args: Array[String]) {
    execute((sparkContext, sqlContext) => {

      import sqlContext.implicits._

      val inputFiles = "/Users/chris/temperature/data/gsod/2008/*.op"
      val outputFile = "/Users/chris/temparature/transformed/temperatures.parquet"

      val lines = sparkContext.textFile(inputFiles).filter(isDataRow);
      val readings = lines.map(Reading.fromCsv).repartition(100)
      readings.toDF.write.mode(Overwrite).parquet(outputFile)

    })
  }

  def isDataRow(line: String) = {
    !line.startsWith("STN")
  }

}

