package temptest

import trainanalysis.spark.BaseSparkJob
import temptest.data.Station
import org.apache.spark.sql.SaveMode.Overwrite

object ProcessStations extends BaseSparkJob {

  def appName = "Process Station Files"

  def main(args: Array[String]) {
    execute((sparkContext, sqlContext) => {

      import sqlContext.implicits._

      val inputFiles = "/Users/chris/temperature/data/stations.txt"
      val outputFile = "/Users/chris/temparature/transformed/stations.parquet"

      val lines = sparkContext.textFile(inputFiles)
      val stations = lines.flatMap(Station.fromCsv)
      stations.toDF.write.mode(Overwrite).parquet(outputFile)
    })
  }
}