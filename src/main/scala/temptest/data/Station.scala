package temptest.data

import org.apache.commons.lang.StringUtils._

case class Station(id: Int, name: String, country: String, lat: Int, long: Int, elev: Int)

object Station {

  def fromCsv(s: String) = {
    val id = s.substring(0, 6)
    val name = s.substring(13, 42)
    val country = s.substring(43, 45)
    val lat = s.substring(58, 64)
    val long = s.substring(65, 71)
    val elev = s.substring(73, 79)

    if (isPresent(id, name, country, lat, long, elev)) {
      Some(Station(id.toInt, name.trim, country, lat.toInt, long.toInt, elev.toInt))
    } else {
      None
    }
  }

  def isPresent(data: String*) = {
    data.forall(x => !isBlank(x))
  }
}