package temptest.data

case class Country(id: String, name: String)

object Country {

  def fromCsv(s: String) = Country(s.substring(0, 2), s.substring(12).trim)

}