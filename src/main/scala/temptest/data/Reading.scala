package temptest.data

import java.sql.Date
import java.time.LocalDate
import java.time.format.DateTimeFormatter

case class Reading(
  stationId: Int,
  date: Date,
  temp: Option[Double],
  dewp: Option[Double],
  slp: Option[Double],
  stp: Option[Double],
  visib: Option[Double],
  wdsp: Option[Double],
  mxspd: Option[Double],
  gust: Option[Double],
  max: Option[Double],
  min: Option[Double],
  precip: Option[Double],
  sndp: Option[Double],
  fog: Boolean,
  rain: Boolean,
  snow: Boolean,
  hail: Boolean,
  thunder: Boolean,
  tornado: Boolean)

object Reading {

  val dateFormat = DateTimeFormatter.ofPattern("yyyyMMdd")

  def fromCsv(s: String) = Reading(
    stationId = s.substring(0, 6).toInt,
    date = Date.valueOf(LocalDate.parse(s.substring(14, 22), dateFormat)),
    temp = parseNumeric(s.substring(24, 30), "9999.9"),
    dewp = parseNumeric(s.substring(35, 41), "9999.9"),
    slp = parseNumeric(s.substring(46, 52), "9999.9"),
    stp = parseNumeric(s.substring(57, 63), "9999.9"),
    visib = parseNumeric(s.substring(68, 73), "999.9"),
    wdsp = parseNumeric(s.substring(78, 83), "999.9"),
    mxspd = parseNumeric(s.substring(88, 93), "999.9"),
    gust = parseNumeric(s.substring(95, 100), "999.9"),
    max = parseNumeric(s.substring(102, 108), "9999.9"),
    min = parseNumeric(s.substring(110, 116), "9999.9"),
    precip = parseNumeric(s.substring(118, 123), "99.9"),
    sndp = parseNumeric(s.substring(125, 130), "999.9"),
    fog = s.charAt(132) == '1',
    rain = s.charAt(133) == '1',
    snow = s.charAt(134) == '1',
    hail = s.charAt(135) == '1',
    thunder = s.charAt(136) == '1',
    tornado = s.charAt(137) == '1')

  def parseNumeric(s: String, absentValue: String) = {
    if (s.equals(absentValue)) None else Some(s.toDouble)
  }

}