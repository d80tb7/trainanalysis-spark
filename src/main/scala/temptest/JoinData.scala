package temptest

import trainanalysis.spark.BaseSparkJob
import temptest.data.Country
import temptest.data.Station
import temptest.data.Reading
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.SQLContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._

object JoinData extends BaseSparkJob {

  def appName = "Join Data"

  def main(args: Array[String]) {
    execute((sparkContext, sqlContext) => {

      import sqlContext.implicits._

      val countryFile = "/Users/chris/temparature/transformed/countries.parquet"
      val stationFile = "/Users/chris/temparature/transformed/stations.parquet"
      val readingsFile = "/Users/chris/temparature/transformed/temperatures.parquet"

      val countries = sqlContext.read.parquet(countryFile).as[Country]
      val stations = sqlContext.read.parquet(stationFile).as[Station]
      val readings = sqlContext.read.parquet(readingsFile).select("stationId", "temp").as[Temp]

      val meanTmps = meanTempByStation(readings, sqlContext);
      val meanTmpsWithStationData = meanTmps.as("mt")
        .joinWith(stations.as("s"), $"mt.stationId" === $"s.id")
        .map(x => Result(x._2.id, x._2.name, x._1.temp))
        .show()
    })
  }

  /*
   * Finds the Mean temp for the whole year by station
   */
  def meanTempByStation(readings: Dataset[Temp], sqlContext: SQLContext) = {
    import sqlContext.implicits._
    readings.groupBy(x => x.stationId)
      .agg(avg($"temp").as[Double])
      .map(x => Temp(x._1, x._2))
  }
}

case class Temp(stationId: Int, temp: Double)
case class Result(stationId: Int, name: String, temp: Double)