/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package trainanalysis.spark

import com.datastax.spark.connector.types.TypeConverter

import trainanalysis.spark.AppConfig._
import trainanalysis.spark.etl.nationrailenquiries.NationalRailScheduleParser
import trainanalysis.spark.etl.nationrailenquiries.NationalRailScheduleProcessor
import trainanalysis.spark.etl.networkrail.model.Tpl
import trainanalysis.spark.process.cassandra.JourneyPersister
import trainanalysis.spark.process.cassandra.RoutePersister
import trainanalysis.spark.process.cassandra.RunDaysToIntConverter

object ProcessNationalRailSchedule extends BaseSparkJob {

  def appName = "Process National Rail Schedules"

  /**
   *  Spark Job to load up the National Rail Schedule files from disk, manipulate them into a set of objects more suitable for our needs
   *  and persist said objects in Cassandra
   */
  def main(args: Array[String]) {

    // We need this so that we can persist the RunDays class to Cassandra
    TypeConverter.registerConverter(RunDaysToIntConverter)

    execute((sparkContext, sqlContext) => {
      import sqlContext.implicits._

      // Create an RDD of schedules from files
      // TODO: line below assumes we have only a single file- extend for multiple files
      val schedulesJson = sparkContext.textFile(nrScheduleFilesInputPattern)
      val allSchedules = schedulesJson.flatMap(x => NationalRailScheduleParser.parse(x))

      // Load up RDD of tpls from files
      val tpls = sqlContext.read.parquet(tplsOutputFile).as[Tpl]
      val uniqueTpls = tpls.distinct.rdd;

      // Convert these into routes and schedules that can be stored in Cassandra
      val consolodatedSchedules = NationalRailScheduleProcessor.consolodate(allSchedules)
      val cassandraObjects = NationalRailScheduleProcessor.toRoutesAndJournies(sparkContext, consolodatedSchedules, uniqueTpls)

      // Store in Cassandra
      val routes = cassandraObjects.map { case (route, journies) => route }
      RoutePersister.persist(cassandraKeyspace, routesTable, routes)

      val journies = cassandraObjects.flatMap { case (route, journies) => journies }
      JourneyPersister.persist(cassandraKeyspace, journiesTable, journies)
    })
  }
}