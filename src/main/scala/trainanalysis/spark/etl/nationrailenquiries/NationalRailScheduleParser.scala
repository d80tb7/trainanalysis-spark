/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */
package trainanalysis.spark.etl.nationrailenquiries

import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.databind.DeserializationFeature._
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.core.JsonToken._
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Parses the schedule information which we get from National Rail (in JSON format) into
 * Scala case classes.  Implemented using Jackson stream processing as the
 * Json files are huge and we don't want to run out of memory!
 */
object NationalRailScheduleParser {

  // Object Mapper use to convert json nodes to scala case classes.
  // This is thread safe so long as we don't try to register extra modules later on etc.
  private lazy val objectMapper = new ObjectMapper()
    .registerModule(DefaultScalaModule)
    .registerModule(new JavaTimeModule())
    .configure(FAIL_ON_UNKNOWN_PROPERTIES, false)

  def parse(json: String): Seq[Schedule] = {
    val jp = objectMapper.getFactory.createParser(json)
    val iter = Iterator.continually(jp.nextToken).takeWhile(_ != null)
    iter.flatMap(x => x match {
      case START_OBJECT => parseSchedule(jp.readValueAsTree())
      case _            => None
    }).toList
  }

  private def parseSchedule(node: JsonNode) = {
    val isSchedule = (node.has("JsonScheduleV1"))
    if (isSchedule) {
      val parsedWrapper = objectMapper.readValue(node.toString, classOf[ScheduleWrapper])
      Some(parsedWrapper.schedule)
    } else {
      None
    }
  }
}
