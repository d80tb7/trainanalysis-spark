/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package trainanalysis.spark.etl.nationrailenquiries

object TrainTypes {

  // Ordinary Passenger Trains
  val londonUnderground = "OL"
  val unadvertisedOrdinaryPassenger = "OU"
  val ordinaryPassenger = "OO"
  val staffTrain = "OS"
  val mixed = "OW"

  // Express Trains
  val channelTunnel = "XC"
  val sleeperEurope = "XD"
  val international = "XI"
  val motoRail = "XR"
  val unadvertisedExpress = "XU"
  val expressPassenger = "XX"
  val sleeperDomestic = "XZ"

  // Buses
  val busReplacement = "BR"
  val busWtt = "BS"

  val allDomesticPassenger = Set(ordinaryPassenger, expressPassenger, sleeperDomestic, busReplacement, busWtt)

}