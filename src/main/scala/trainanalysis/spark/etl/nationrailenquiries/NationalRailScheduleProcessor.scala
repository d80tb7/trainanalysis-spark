/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */
package trainanalysis.spark.etl.nationrailenquiries

import org.apache.spark.rdd.RDD
import trainanalysis.spark.etl.networkrail.model.Tpl
import org.apache.spark.SparkContext
import trainanalysis.spark.etl.util.DateTimeUtil._
import scala.collection.mutable.ListBuffer
import scala.collection.Map
import trainanalysis.spark.process.cassandra.RunDays
import trainanalysis.spark.process.cassandra.Route
import trainanalysis.spark.process.cassandra.Journey

object NationalRailScheduleProcessor {

  def consolodate(schedules: RDD[Schedule]): RDD[Schedule] = {

    // For now only care about non-overlay schedules
    val nonOverlaySchedules = schedules.filter(x => x.stpIndicator == "N" || x.stpIndicator == "P")

    // Filter out anything that either isn't a passenger train or has no passenger stops
    val schedulesWithStops = nonOverlaySchedules.filter(x => x.isPassengerTrain && !x.passengerStops.isEmpty)

    // Finally merge together schedules without differences in any of the fields we care about
    deduplicate(schedulesWithStops)
  }

  def toRoutesAndJournies(sc: SparkContext, schedules: RDD[Schedule], uniqueTpls: RDD[Tpl]): RDD[(Route, Seq[Journey])] = {

    // Broadcast out a map of tpl -> crs as it's small and so quicker than doing a join
    val stations = uniqueTpls.filter(_.isStation).map(x => (x.tpl, x.crs.get)).collectAsMap
    val crsToTpl = sc.broadcast(stations).value

    // filter out anything where we can't match the station
    val schedulesWithStations = schedules.filter {
      x => x.passengerStops.forall(x => crsToTpl.contains(x.tpl))
    }

    // Convert the Schedule into a route and a list of journies 
    schedulesWithStations.map(x => toRouteAndJournies(x, crsToTpl))
  }

  private def toRouteAndJournies(sched: Schedule, crsToTpl: Map[String, String]) = {

    // Route 
    val start = sched.passengerStops.head
    val end = sched.passengerStops.last
    val route = Route(sched.uid,
      crsToTpl.get(start.tpl).get,
      crsToTpl.get(end.tpl).get,
      localDateToDate(sched.startDate),
      localDateToDate(sched.endDate),
      sched.runDays,
      localTimeToTimestamp(start.depart),
      localTimeToTimestamp(end.arrive),
      sched.toc)

    // Journies
    val jouniesBuffer = new ListBuffer[Journey]()
    for (startStationIndex <- 0 to sched.passengerStops.length - 2) {
      for (endStationIndex <- startStationIndex + 1 to sched.passengerStops.length - 1) {
        val startStation = sched.passengerStops(startStationIndex)
        val endStation = sched.passengerStops(endStationIndex)
        jouniesBuffer += Journey(route,
          crsToTpl.get(startStation.tpl).get,
          crsToTpl.get(endStation.tpl).get,
          localTimeToTimestamp(startStation.depart),
          localTimeToTimestamp(endStation.arrive))
      }
    }
    (route, jouniesBuffer.toList)
  }

  // De-duplicate.
  // We do this because the schedule file will contain separate entries if 
  // e.g. the train stops at different platforms on different days  Our rule is that if
  // it stops at the same station at the same time each day then it's the same train.
  private def deduplicate(schedules: RDD[Schedule]): RDD[Schedule] = {
    val schedulesByUid = schedules.groupBy(x => (x.uid, x.passengerStops))
    schedulesByUid.map {
      case ((uid, stops), schedules) => merge(schedules.toList)
    }
  }

  private def merge(schedules: Seq[Schedule]): Schedule = {
    val default = schedules.head
    if (schedules.length > 1) {
      val startDate = schedules.minBy(_.startDate.toEpochDay).startDate
      val endDate = schedules.maxBy(_.endDate.toEpochDay).endDate
      val runDays = schedules.foldLeft(RunDays(0))((x, y) => x | y.runDays)
      Schedule(default.uid, default.toc, startDate, endDate, runDays, default.stpIndicator, default.scheduleSegment)
    } else {
      default
    }
  }
}