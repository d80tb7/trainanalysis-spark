package trainanalysis.spark.etl.nationrailenquiries

object StpIndicators {
  val cancellation = "C"
  val newSched = "N"
  val overlay = "O"
  val permanent = "P"
}