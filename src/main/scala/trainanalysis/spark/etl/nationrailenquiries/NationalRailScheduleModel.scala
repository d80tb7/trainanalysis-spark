/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package trainanalysis.spark.etl.nationrailenquiries

import java.time.LocalTime
import java.time.LocalDate
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.JsonDeserializer
import trainanalysis.spark.process.cassandra.RunDays

case class ScheduleWrapper(@JsonProperty("JsonScheduleV1") schedule: Schedule)

case class Schedule(
    @JsonProperty("CIF_train_uid") uid: String,
    @JsonProperty("atoc_code") toc: String,
    @JsonProperty("schedule_start_date") startDate: LocalDate,
    @JsonProperty("schedule_end_date") endDate: LocalDate,
    @JsonProperty("schedule_days_runs")@JsonDeserialize(using = classOf[RunDaysDeserializer]) runDays: RunDays,
    @JsonProperty("CIF_stp_indicator") stpIndicator: String,
    @JsonProperty("schedule_segment") scheduleSegment: ScheduleSegment) {

  lazy val passengerStops: Seq[ScheduleLocation] = scheduleSegment.locations.filter {
    x => Set("LO", "LP", "LT").contains(x.locationType)
  }

  lazy val isPassengerTrain: Boolean = TrainTypes.allDomesticPassenger.contains(scheduleSegment.trainCategory)

}

case class ScheduleSegment(
  @JsonProperty("CIF_train_category") trainCategory: String,
  @JsonProperty("schedule_location") locations: Seq[ScheduleLocation])

case class ScheduleLocation(
    @JsonProperty("tiploc_code") tpl: String,
    @JsonProperty("location_type") locationType: String,
    @JsonProperty("public_arrival")@JsonFormat(pattern = "HHmm") rawArrive: Option[LocalTime],
    @JsonProperty("public_departure")@JsonFormat(pattern = "HHmm") rawDepart: Option[LocalTime]) {

  def arrive = rawArrive.getOrElse(rawDepart.get)

  def depart = rawDepart.getOrElse(rawArrive.get)

}

class RunDaysDeserializer extends JsonDeserializer[RunDays] {
  override def deserialize(jp: JsonParser, ctxt: DeserializationContext): RunDays = {
    val chars = jp.getValueAsString.toCharArray
    val dayFlags = chars.foldLeft(0) { (x, y) =>
      val flag = if (y == '1') 1 else 0
      x << 1 | flag
    }
    RunDays(dayFlags);
  }
}
