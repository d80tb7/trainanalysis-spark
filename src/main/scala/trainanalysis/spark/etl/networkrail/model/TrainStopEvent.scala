/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package trainanalysis.spark.etl.networkrail.model

import java.sql.Date
import java.time.LocalDate
import java.time.LocalTime
import java.sql.Timestamp
import trainanalysis.spark.etl.util.DateTimeUtil._

case class TrainStopEvent(ssd: Date, uid: String, tpl: String, eventTime: Timestamp, eventType: Int)

object TrainStopEvent {

  val ARRIVE = 1
  val DEPART = 2

  def arrival(ssd: Date, uid: String, tpl: String, eventTime: Timestamp): TrainStopEvent = {
    TrainStopEvent(ssd, uid, tpl, eventTime, ARRIVE)
  }

  def departure(ssd: Date, uid: String, tpl: String, eventTime: Timestamp): TrainStopEvent = {
    new TrainStopEvent(ssd, uid, tpl, eventTime, DEPART)
  }

}

