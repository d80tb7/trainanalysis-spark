/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package trainanalysis.spark.etl.networkrail.model

import java.sql.Date
import trainanalysis.spark.etl.util.DateTimeUtil._

/** Network Rail Train Operating Company */
case class Toc(code: String, name: String)

/** Network Rail Train Passing Location */
case class Tpl(tpl: String, crs: Option[String], toc: Option[String], locName: String) {
  def isStation: Boolean = crs.isDefined && toc.isDefined
}

/**  Container which simply holds TOCs and TPLs for a given date. */
case class RefData(ssdRaw: Date, tpls: Seq[Tpl], tocs: Seq[Toc]) {
  lazy val ssd = dateToLocalDate(ssdRaw)
}

