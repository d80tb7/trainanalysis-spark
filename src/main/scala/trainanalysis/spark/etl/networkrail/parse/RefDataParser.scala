/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package trainanalysis.spark.etl.networkrail.parse

import scala.collection.JavaConversions._
import trainanalysis.spark.etl.networkrail.model.RefData
import trainanalysis.spark.etl.networkrail.model.Toc
import trainanalysis.spark.etl.networkrail.model.Tpl
import trainanalysis.darwin.model.ctt.referenceschema.PportTimetableRef
import java.sql.Date
import java.time.LocalDate

/**
 * Parser for Network Rail Reference Data files.
 * From this we parse out the List of Train Passing Locations (TPLs)
 * and Train Operating Companies (TOCs) that are valid for a given schedule
 */
object RefDataParser extends JaxBParser[PportTimetableRef, Seq[RefData]] {
 
  override def convert(refData: PportTimetableRef): Seq[RefData] = {
    val ssd  = parseSsd(refData.getTimetableId)
    val tocs = extractTocs(refData)
    val tpls = extractTpls(refData)
    Seq(new RefData(ssd, tpls, tocs));
  }

  private def extractTocs(refData: PportTimetableRef) = refData.getTocRef.map { x =>
    new Toc(x.getToc, x.getTocname)
  }

  private def extractTpls(refData: PportTimetableRef) = refData.getLocationRef.map { x =>
    new Tpl(x.getTpl, Option(x.getCrs), Option(x.getToc), x.getLocname)
  }

  private def parseSsd(timetableId: String) = {
    val date = timetableId.substring(0, 8);  // yyyymmdd
    val year = date.substring(0,4).toInt
    val month =  date.substring(4,6).toInt
    val day =  date.substring(6,8).toInt
    new Date(year - 1900, month - 1, day)
  }

}


