/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */
package trainanalysis.spark.etl.networkrail.parse

import trainanalysis.darwin.model.ppt.schema.Pport
import javax.xml.bind.Unmarshaller
import java.io.StringReader
import org.slf4j.LoggerFactory
import javax.xml.bind.JAXBContext
import trainanalysis.darwin.model.ppt.forecasts.TS
import trainanalysis.darwin.model.ppt.schema.Pport
import scala.collection.JavaConversions.asScalaBuffer
import trainanalysis.spark.etl.util.DateTimeUtil._
import trainanalysis.darwin.model.ppt.forecasts.TSLocation
import java.time.LocalDate
import trainanalysis.spark.etl.networkrail.model.TrainStopEvent
import java.sql.Date
import com.typesafe.scalalogging.LazyLogging

/**
 * Parse for Network Rail pPort Event files.
 * These files contain multiple xml documents and so need to be pre-split before processing
 */
object TrainEventParser extends JaxBParser[Pport, Seq[TrainStopEvent]] with LazyLogging {

  /** Regex for start of xml doc */
  private val xmlStartRegex = "\\<\\?xml(.+?)\\?\\>";

  override def parse(content: String): Seq[TrainStopEvent] = {
    // Event files contain multiple xml docs
    // Therefore split docs before processing each individually
    val messages = content.split(xmlStartRegex).filterNot(_.trim.isEmpty)
    val messagesWithStopTime = messages.filter(_.contains("at="))
    messagesWithStopTime.flatMap { doc =>
      try {
        super.parse(doc)
      } catch {
        case t: Throwable => {
          logger.warn("Error parsing line {}.", doc)
          List();
        }
      }
    }
  }

  protected override def convert(pp: Pport): Seq[TrainStopEvent] = {
    val ts = pp.getUR.getTS
    ts.flatMap { x =>
      val ssd = xmlGregorianCalToDate(x.getSsd)
      val uid = x.getUid();
      x.getLocation.flatMap { loc =>
        convertToStopEvent(ssd, uid, loc)
      }
    }
  }

  private def convertToStopEvent(ssd: Date, uid: String, loc: TSLocation): Seq[TrainStopEvent] = {

    val hasArrive = loc.isSetArr && loc.getArr.isSetAt
    val hasDepart = loc.isSetDep && loc.getDep.isSetAt

    val arrived = hasArrive match {
      case true  => Some(TrainStopEvent.arrival(ssd, uid, loc.getTpl, parseTimeAsTimestamp(loc.getArr.getAt).get))
      case false => None
    }
    val departed = hasDepart match {
      case true  => Some(TrainStopEvent.departure(ssd, uid, loc.getTpl, parseTimeAsTimestamp(loc.getDep.getAt).get))
      case false => None
    }
    (arrived ++ departed).toList;
  }
}
