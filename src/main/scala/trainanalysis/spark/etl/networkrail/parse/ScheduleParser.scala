/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package trainanalysis.spark.etl.networkrail.parse

import java.io.StringReader
import scala.collection.JavaConversions.asScalaBuffer
import trainanalysis.darwin.model.ctt.schema.DT
import trainanalysis.darwin.model.ctt.schema.IP
import trainanalysis.darwin.model.ctt.schema.OPDT
import trainanalysis.darwin.model.ctt.schema.OPOR
import trainanalysis.darwin.model.ctt.schema.OR
import trainanalysis.darwin.model.ctt.schema.PportTimetable
import trainanalysis.darwin.model.ctt.schema.PportTimetable
import javax.xml.bind.JAXBContext
import trainanalysis.spark.etl.networkrail.model.PlannedStop
import trainanalysis.spark.etl.networkrail.model.Schedule
import trainanalysis.spark.etl.util.DateTimeUtil._
import trainanalysis.spark.etl.util.DateTimeUtil._
import scala.reflect.ClassTag

/**
 * Parser for the Network Rail pPort Schedule files.
 * From this we parse out the *Scheduled* arrival and departure time
 * of each train at each TPL for a given date.
 *
 * At this point we do not consider trains that split and merge.
 */
object ScheduleParser extends JaxBParser[PportTimetable, Seq[Schedule]] {

  private val passengerTrainTypes = Set("OL", "OO", "OW", "XC", "XD", "XI", "XR", "XX", "XZ")

  override def convert(timetable: PportTimetable): Seq[Schedule] = {
    timetable.getJourney.map(convertToSchedule)
  }

  private def convertToSchedule(pportSchedule: trainanalysis.darwin.model.ctt.schema.Schedule): Schedule = {

    val uid = pportSchedule.getUid
    val ssd = xmlGregorianCalToDate(pportSchedule.getSsd)
    val toc = pportSchedule.getToc
    val passengerService = isPassengerService(pportSchedule)

    // Assumption here is that pPort xml has stops in correct order.
    val stops = pportSchedule.getOROrOPOROrIP().flatMap { x =>
      x match {
        case x: OR   => Some(PlannedStop(x.getTpl, None, parseTimeAsTimestamp(x.getPtd)))
        case x: OPOR => Some(PlannedStop(x.getTpl, None, parseTimeAsTimestamp(x.getWtd)))
        case x: IP   => Some(PlannedStop(x.getTpl, parseTimeAsTimestamp(x.getPta), parseTimeAsTimestamp(x.getPtd)))
        case x: OPDT => Some(PlannedStop(x.getTpl, parseTimeAsTimestamp(x.getWta), None))
        case x: DT   => Some(PlannedStop(x.getTpl, parseTimeAsTimestamp(x.getPta), None))
        case _       => None
      }
    }
    new Schedule(ssd, uid, toc, passengerService, stops);
  }

  private def isPassengerService(pportSchedule: trainanalysis.darwin.model.ctt.schema.Schedule): Boolean = {
    val trainCat = pportSchedule.getTrainCat;
    passengerTrainTypes.contains(trainCat) match {
      case true  => !pportSchedule.isSetIsPassengerSvc || pportSchedule.isIsPassengerSvc
      case false => false
    }
  }

}
