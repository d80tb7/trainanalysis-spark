/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package trainanalysis.spark.etl.networkrail.parse

import javax.xml.bind.JAXBContext
import trainanalysis.darwin.model.ctt.referenceschema.PportTimetableRef
import scala.reflect.ClassTag
import scala.reflect._
import java.io.StringReader
import javax.xml.stream.XMLInputFactory

/**
 * Base functionality for processing National Rail XML files via JaxB
 */
abstract class JaxBParser[JaxBClass: ClassTag, OutputClass] {

  /** JaxB context is expensive to create but is threadsafe so just create one */
  private val jaxBContext = JAXBContext.newInstance(classTag[JaxBClass].runtimeClass)

  private val xmlInputFactory = XMLInputFactory.newInstance()

  /**
   * Parse the given xml string using JaxB and convert into custom type
   * Note that xml string should be a single, complete xml doc that can be marshalled into [JaxBClass]
   */
  def parse(xml: String): OutputClass = {
    val unmarshaller = jaxBContext.createUnmarshaller
    val reader = xmlInputFactory.createXMLStreamReader(new StringReader(xml));
    val jaxbRoot = unmarshaller.unmarshal(reader).asInstanceOf[JaxBClass]
    convert(jaxbRoot)
  }

  /**
   * Converter function to be implemented by subclasses
   */
  protected def convert(input: JaxBClass): OutputClass

}
