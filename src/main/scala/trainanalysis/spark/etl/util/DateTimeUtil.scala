/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package trainanalysis.spark.etl.util

import java.sql.Timestamp
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import javax.xml.datatype.XMLGregorianCalendar
import java.time.LocalDate
import java.sql.Date
import java.util.concurrent.TimeUnit._
import java.time.ZonedDateTime

/**
 * Some utility converters to deal with the fact that
 * we have to cope with dates and ties in half a dozen different formats
 */
object DateTimeUtil {

  private val formatter = DateTimeFormatter.ofPattern("HH:mm[:ss]")

  // This is used as the cut off period for deciding if a time has crossed a midnight boundary.
  private val fourHoursInMs = MILLISECONDS.convert(4, HOURS)

  def parseTime(timeStr: String): LocalTime = {
    LocalTime.parse(timeStr, formatter)
  }

  def parseTimeAsTimestamp(timeStr: String): Option[Timestamp] = {
    val timeOpt = Option(timeStr)
    timeOpt.map { x =>
      localTimeToTimestamp(parseTime(x))
    }
  }

  def timestampToLocalTime(ts: Timestamp): LocalTime = {
    LocalTime.ofSecondOfDay(SECONDS.convert(ts.getTime, MILLISECONDS))
  }

  def localTimeToTimestamp(lt: LocalTime): Timestamp = {
    new Timestamp(MILLISECONDS.convert(lt.toSecondOfDay, SECONDS))
  }

  def dateToLocalDate(date: Date): LocalDate = {
    date.toLocalDate()
  }

  def localDateToDate(ld: LocalDate): Date = {
    Date.valueOf(ld);
  }

  def xmlGregorianCalToLocalDate(xmlCal: XMLGregorianCalendar): LocalDate = {
    xmlCal.toGregorianCalendar.toZonedDateTime.toLocalDate;
  }

  def xmlGregorianCalToDate(xmlCal: XMLGregorianCalendar): Date = {
    localDateToDate(xmlGregorianCalToLocalDate(xmlCal));
  }

  /*
   * Converts a timestamp (time only) into a timestamp (datetime) based on the start time of the train.
   * Rule here is that if the timestamp is more than four hours before the start time of the train then 
   * we assume we've moved into the next day.  This will be wrong in the following (unlikely) situations:
   * a) Train has started more than four hours before schedule says it should.
   * b) Train is still producing timestamps more than 20 hours after it was supposed to start.
   * 
   * Unfortunately Network Rail only give us times (not datetimes) so this is about the best we can do.
   */
  def createTimestampFromTime(time: Timestamp, ssd: Date, startTime: Timestamp): Timestamp = {

    val isNextDay = (startTime.getTime - time.getTime > fourHoursInMs)
    val baseDate = ssd.toLocalDate
    val actualDate = if (isNextDay) baseDate.plusDays(1) else baseDate
    new Timestamp(actualDate.getYear - 1900, actualDate.getMonthValue, actualDate.getDayOfMonth,
      time.getHours, time.getMinutes, time.getSeconds, time.getNanos)
  }
}
