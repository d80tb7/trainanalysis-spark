/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package trainanalysis.spark

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import java.util.concurrent.TimeUnit._
import System.currentTimeMillis
import org.apache.spark.sql.SQLContext

/**
 * Some base functionality for classes that are spark jobs
 */
abstract class BaseSparkJob extends LazyLogging {

  protected def appName: String

  /**
   * Creates spark and spark sql contexts and then executes the job according to the provided function.
   */
  protected def execute(jobFunc: (SparkContext, SQLContext) => Unit) {

    val startTime = currentTimeMillis;

    // Setup spark context
    val sparkConf = new SparkConf().setAppName(appName)
    val sc = new SparkContext(sparkConf)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)

    // Run actual spark job
    jobFunc(sc, sqlContext)

    // Log out time taken
    val timeTaken = SECONDS.convert(currentTimeMillis - startTime, MILLISECONDS)
    logger.info("Finished job {} in {} seconds", appName, timeTaken.toString)
  }

}