/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */
package trainanalysis.spark

import scala.reflect.ClassTag
import scala.reflect.runtime.universe

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SaveMode.Overwrite

import com.typesafe.scalalogging.LazyLogging

import trainanalysis.spark.AppConfig.eventFilesInputPattern
import trainanalysis.spark.AppConfig.eventsOutputFile
import trainanalysis.spark.AppConfig.refDataFilesInputPattern
import trainanalysis.spark.AppConfig.scheduleFilesInputPattern
import trainanalysis.spark.AppConfig.schedulesOutputFile
import trainanalysis.spark.AppConfig.tocsOutputFile
import trainanalysis.spark.AppConfig.tplsOutputFile
import trainanalysis.spark.etl.networkrail.parse.RefDataParser
import trainanalysis.spark.etl.networkrail.parse.ScheduleParser
import trainanalysis.spark.etl.networkrail.parse.TrainEventParser

/**
 * Spark Job which takes all Network Rail XML files (Ref Data, Schedules, and Train Events),
 * parses them into something a bit easier to work with and saves them onto HDFS
 * (in parquet format) for later processing.
 */
object ImportNetworkRailFiles extends BaseSparkJob {
  
  def appName = "Import Network Rail Files"

  def main(args: Array[String]) {

    execute((sparkContext, sqlContext) => {

      import sqlContext.implicits._

      // RefData
      logger.info("Processing ref data files")
      val refData = createRdd(sparkContext, refDataFilesInputPattern, RefDataParser.parse)
      refData.flatMap(x => x.tpls).toDF.write.mode(Overwrite).parquet(tplsOutputFile)
      refData.flatMap(x => x.tocs).toDF.write.mode(Overwrite).parquet(tocsOutputFile)

      // Schedules
      logger.info("Processing schedule files")
      val schedules = createRdd(sparkContext, scheduleFilesInputPattern, ScheduleParser.parse)
      schedules.toDF.write.mode(Overwrite).parquet(schedulesOutputFile)

      // Events
      logger.info("Processing train event files")
      val events = createRdd(sparkContext, eventFilesInputPattern, TrainEventParser.parse)
      events.toDF.write.mode(Overwrite).parquet(eventsOutputFile)

    })
  }

  /**
   * Parse a number of XMl files into an RDD using the supplied parse function.
   */
  private def createRdd[T: ClassTag](sc: SparkContext, inputFilePattern: String, parseFunc: (String) => Seq[T]): RDD[T] = {
    val allFiles = sc.wholeTextFiles(inputFilePattern)
    allFiles.flatMap {
      case (path, content) => {
        logger.info("Processing file {}", path)
        parseFunc.apply(content)
      }
    }
  }
}
