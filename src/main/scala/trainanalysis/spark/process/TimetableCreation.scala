/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package trainanalysis.spark.process

import java.sql.Date
import java.sql.Timestamp

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.rdd.RDD.rddToPairRDDFunctions

import trainanalysis.spark.etl.networkrail.model.Schedule
import trainanalysis.spark.etl.networkrail.model.Tpl
import trainanalysis.spark.etl.networkrail.model.TrainStopEvent
import trainanalysis.spark.etl.networkrail.model.TrainStopEvent.ARRIVE
import trainanalysis.spark.etl.networkrail.model.TrainStopEvent.DEPART
import trainanalysis.spark.etl.util.DateTimeUtil.createTimestampFromTime
import trainanalysis.spark.process.model.Timetable
import trainanalysis.spark.process.model.TimingKey

/**
 * Converts the individual RDDs parsed from the Network Rail files (schedules, events and stations) into a single unified RDD of Timetable.
 * This timetable RDD contains the train id (uid), station (both tpl and crs) and the actual and scheduled arrival and depart times.
 */
object TimetableCreation {

  private val defaultNumPartitions = 50;

  def createTimetable(
    sc: SparkContext,
    schedules: RDD[Schedule],
    events: RDD[TrainStopEvent],
    uniqueTpls: RDD[Tpl],
    numPartitons: Int = defaultNumPartitions): RDD[Timetable] = {

    // Key everything by a timing key and partition with same partitioner.
    val partitioner = new TimingKeyPartitioner(numPartitons);
    val eventsByKey = events.keyBy(x => TimingKey(x.uid, x.ssd, x.tpl)).partitionBy(partitioner)
    val scheduledStops = schedules.filter(_.isPassengerService).flatMap(scheduleToScheduledStops)
    val scheduledStopsByKey = scheduledStops.keyBy(x => TimingKey(x.uid, x.ssd, x.tpl)).partitionBy(partitioner)

    // Convert train events to actual stop times
    val actualTimes = timingEventsToActualStopTimes(eventsByKey)

    // Join actual stop times with stations (to get the CRS). Done as broadcast variable as stations should be relatively small.
    val stations = uniqueTpls.filter(_.isStation).map(x => (x.tpl, x.crs.get)).collectAsMap
    val stationsBroadcast = sc.broadcast(stations)
    val scheduledTimes = scheduledStopsByKey.mapPartitions({ part =>
      part.flatMap {
        case (key, sched) =>
          val crs = stationsBroadcast.value.get(key.tpl)
          crs.map { x => (key, ScheduledTimes(sched.stopNum, x, sched.scheduledArrive, sched.scheduledDepart, sched.startTime)) }
      }
    }, preservesPartitioning = true)

    // Finally convert to RDD of Timetable
    scheduledTimes.leftOuterJoin(actualTimes).map {
      case (key, (scheduled, actual)) =>
        val schedArrival = scheduled.arrive.map(t => createTimestampFromTime(t, key.ssd, scheduled.startTime))
        val schedDepart = scheduled.depart.map(t => createTimestampFromTime(t, key.ssd, scheduled.startTime))
        val actArrival = actual.flatMap(a => a.arrive.map(t => createTimestampFromTime(t, key.ssd, scheduled.startTime)))
        val actDepart = actual.flatMap(a => a.depart.map(t => createTimestampFromTime(t, key.ssd, scheduled.startTime)))
        Timetable(key.uid, key.ssd, scheduled.crs, key.tpl, scheduled.stopNum, scheduled.arrive, scheduled.depart, actArrival, actDepart)
    }
  }

  private def timingEventsToActualStopTimes(events: RDD[(TimingKey, TrainStopEvent)]) = {
    // Extract arrival and departure events respectively
    val arrivalEvents = events.filter { case (key, event) => event.eventType == ARRIVE }
    val departEvents = events.filter { case (key, event) => event.eventType == DEPART }

    // Depdupe events
    val dedupedArrivals = arrivalEvents.reduceByKey((k, v) => v);
    val dedupedDeparts = departEvents.reduceByKey((k, v) => v);

    dedupedArrivals.leftOuterJoin(dedupedDeparts).map {
      case (key, (arrive, depart)) => (key, ActualTimes(Some(arrive.eventTime), depart.map(_.eventTime)))
    }
  }

  private def scheduleToScheduledStops(sched: Schedule) = sched.stops match {
    case Nil => Nil
    case _ =>
      {
        val startTime = sched.stops.head.depart.getOrElse(sched.stops.head.arrive.get)
        sched.stops.zipWithIndex.map {
          case (stop, index) =>
            ScheduledStop(sched.uid, sched.ssd, stop.tpl, index, stop.arrive, stop.depart, startTime)
        }
      }
  }

  // some classes that are useful for the intermediate objects when we transform RDDs
  private case class ActualTimes(arrive: Option[Timestamp], depart: Option[Timestamp])
  private case class ScheduledTimes(stopNum: Int, crs: String, arrive: Option[Timestamp], depart: Option[Timestamp], startTime: Timestamp)
  private case class ScheduledStop(
    uid: String,
    ssd: Date,
    tpl: String,
    stopNum: Int,
    scheduledArrive: Option[Timestamp],
    scheduledDepart: Option[Timestamp],
    startTime: Timestamp)
}
