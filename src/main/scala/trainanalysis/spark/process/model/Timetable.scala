/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package trainanalysis.spark.process.model

import java.sql.Timestamp
import java.sql.Date

case class Timetable(uid: String, ssd: Date, crs: String, tpl: String, stopNum: Int, scheduledarrive: Option[Timestamp],
                     scheduleddepart: Option[Timestamp], actualarrive: Option[Timestamp], actualdepart: Option[Timestamp]) {

  def arrivalDelta: Option[Int] = delta(scheduledarrive, actualarrive)

  def departDelta: Option[Int]  = delta(scheduleddepart, actualdepart)

  private def delta(scheduled: Option[Timestamp], actual: Option[Timestamp]) = {
    val allData = scheduled.isDefined && actual.isDefined
    allData match {
      case true  => Some(actual.get.getTime - scheduled.get.getTime).map(_.intValue)
      case false => None
    }
  }
}
