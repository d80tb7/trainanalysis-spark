package trainanalysis.spark.process.cassandra

import java.sql.Timestamp
import java.sql.Date
import collection.JavaConverters._

case class Journey(route: Route, startCrs: String, endCrs: String, startTime: Timestamp, endTime: Timestamp)

case class Route(uid: String, startCrs: String, endCrs: String, validFrom: Date, validTo: Date,
                 runDays: RunDays, startTime: Timestamp, endTime: Timestamp, toc: String)

case class RunDays(dayFlags: Int) {

  def &(that: RunDays): RunDays = new RunDays(this.dayFlags & that.dayFlags)

  def |(that: RunDays): RunDays = new RunDays(this.dayFlags | that.dayFlags)
}
