/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */
package trainanalysis.spark.process.cassandra

import scala.reflect.runtime.universe
import scala.reflect.runtime.universe.typeTag

import org.apache.spark.rdd.RDD

import com.datastax.spark.connector.mapper.DefaultColumnMapper
import com.datastax.spark.connector.toRDDFunctions
import com.datastax.spark.connector.types.TypeConverter

/**
 * Functions to Persist RDDs of Route s to Cassandra
 */
object RoutePersister {

  private implicit object Mapper extends DefaultColumnMapper[Route](
    Map(
      "validFrom" -> "validfrom",
      "validTo" -> "validto",
      "runDays" -> "rundays",
      "endTime" -> "destinationarrive",
      "endCrs" -> "destinationtpl",
      "startTime" -> "origindepart",
      "startCrs" -> "origintpl"))

  def persist(keySpace: String, table: String, routes: RDD[Route]) {
    routes.saveToCassandra(keySpace, table)
  }
}

// Typeconverter so that cassandra connector can persist a rundays as the underlying dayflags
object RunDaysToIntConverter extends TypeConverter[Integer] {
  def targetTypeTag = typeTag[Integer]
  def convertPF = { case RunDays(dayFlags) => dayFlags }
}