/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package trainanalysis.spark.process.cassandra

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import com.datastax.spark.connector.writer.RowWriterFactory
import com.datastax.spark.connector.ColumnRef
import com.datastax.spark.connector.cql.TableDef
import com.datastax.spark.connector.writer.RowWriter
import com.datastax.spark.connector._

object JourneyPersister {

  private implicit object JourneyWriterFactory extends RowWriterFactory[Journey] {
    def rowWriter(table: TableDef, selectedColumns: IndexedSeq[ColumnRef]) = {
      new JourneyWriter()
    }
  }

  def persist(keySpace: String, table: String, journies: RDD[Journey]) {
    journies.saveToCassandra(keySpace, table)
  }

}

private class JourneyWriter extends RowWriter[Journey] {

  def columnNames: Seq[String] = {
    Seq(
      "startcrs",
      "endcrs",
      "uid",
      "starttime",
      "endtime",
      "origintpl",
      "destinationtpl",
      "validfrom",
      "validto",
      "rundays",
      "validfromindex",
      "validtoindex",
      "origindepart",
      "destinationarrive",
      "toc")
  }

  def readColumnValues(data: Journey, buffer: Array[Any]) = {
    buffer(0) = data.startCrs
    buffer(1) = data.endCrs
    buffer(2) = data.route.uid
    buffer(3) = data.startTime
    buffer(4) = data.endTime
    buffer(5) = data.route.startCrs
    buffer(6) = data.route.endCrs
    buffer(7) = data.route.validFrom
    buffer(8) = data.route.validTo
    buffer(9) = data.route.runDays.dayFlags
    buffer(10) = data.route.validFrom
    buffer(11) = data.route.validTo
    buffer(12) = data.route.startTime
    buffer(13) = data.route.endTime
    buffer(14) = data.route.toc
  }
}