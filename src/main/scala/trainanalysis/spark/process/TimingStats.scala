/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package trainanalysis.spark.process

import trainanalysis.spark.process.model.Timetable
import org.apache.spark.rdd.RDD
import java.time.LocalDate
import trainanalysis.spark.process.model.CalculatedTimingStat
import trainanalysis.spark.process.model.CalculatedTimingStat
import trainanalysis.spark.process.model.CalculatedTimingStat

object TimingStats {

  def createStatsByUid(timetable: RDD[Timetable], time: LocalDate): RDD[CalculatedTimingStat] = {

    val aYearAgo = time.minusYears(1)
    val aMonthAgo = time.minusMonths(1);
    val aWeekAgo = time.minusWeeks(1);

    val timeTableByUid = timetable.groupBy(x => (x.uid, x.tpl))
    timeTableByUid.map {
      case ((uid, tpl), timingInfo) => {
        val avOverYear = calcAverageDelta(timingInfo, aYearAgo)
        val avOverMonth = calcAverageDelta(timingInfo, aMonthAgo)
        val avOverWeek = calcAverageDelta(timingInfo, aWeekAgo)
        CalculatedTimingStat(uid, tpl, avOverWeek, avOverMonth, avOverYear)
      }
    }
  }

  private def calcAverageDelta(stats: Iterable[Timetable], cutoff: LocalDate): Int = {
    val deltas = stats.filter(_.ssd.toLocalDate.isAfter(cutoff)).flatMap(_.arrivalDelta).toSeq
    deltas match {
      case Nil => 0
      case _   => (deltas.sum / (deltas.length)).intValue
    }
  }

}
