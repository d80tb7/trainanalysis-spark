/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package trainanalysis.spark

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD.rddToOrderedRDDFunctions

import com.typesafe.scalalogging.LazyLogging

import trainanalysis.spark.AppConfig.cassandraAddr
import trainanalysis.spark.AppConfig.eventsOutputFile
import trainanalysis.spark.AppConfig.schedulesOutputFile
import trainanalysis.spark.AppConfig.tocsOutputFile
import trainanalysis.spark.AppConfig.tplsOutputFile
import trainanalysis.spark.etl.networkrail.model.Schedule
import trainanalysis.spark.etl.networkrail.model.Toc
import trainanalysis.spark.etl.networkrail.model.Tpl
import trainanalysis.spark.etl.networkrail.model.TrainStopEvent
import trainanalysis.spark.process.TimetableCreation
import trainanalysis.spark.AppConfig._
import com.datastax.spark.connector._

/**
 * Spark job which takes the Network Rail Parquet files produced by ETC (Tocs, Tpls etc) and 
 * processes them into the various Cassandra tables required by the webapi.  
 */
object ProcessNetworkRailData extends BaseSparkJob {
  
  def appName = "Process Network Rail Data"

  def main(args: Array[String]) {

    execute((sparkContext, sqlContext) => {

      import sqlContext.implicits._

      // Load DataSets
      val tocs = sqlContext.read.parquet(tocsOutputFile).as[Toc]
      val tpls = sqlContext.read.parquet(tplsOutputFile).as[Tpl]
      val schedules = sqlContext.read.parquet(schedulesOutputFile).as[Schedule]
      val trainEvents = sqlContext.read.parquet(eventsOutputFile).as[TrainStopEvent]

      // Convert tocs and tpls to unique values- we don't care about per day.
      val uniqueTocs = tocs.distinct;
      val uniqueTpls = tpls.distinct;

      // Create a Timetable and cache it
      val timeTable = TimetableCreation.createTimetable(sparkContext, schedules.rdd, trainEvents.rdd, uniqueTpls.rdd).cache

      // Sort the timetable by Cassandra partition key (makes it faster to store)
      val sortedTimeTable = timeTable.mapPartitions({
        x => x.toList.sortBy(_.uid).toIterator
      }, true)

      // Save tables to Cassandra
      sortedTimeTable.saveToCassandra(cassandraKeyspace, timetablesTable)
      uniqueTocs.rdd.saveToCassandra(cassandraKeyspace, timetablesTable)
      uniqueTpls.rdd.saveToCassandra(cassandraKeyspace, timetablesTable)

    })
  }

}
