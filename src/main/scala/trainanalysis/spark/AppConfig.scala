/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */
package trainanalysis.spark

import com.typesafe.config.ConfigFactory

/*
 * Configuration for the spark jobs.  See values in application.conf
 */
object AppConfig {

  private val config = ConfigFactory.load()

  // Input
  val inputDir = config.getString("inputDir")
  val scheduleFilesInputPattern = inputDir + "/" + config.getString("scheduleFilesInputPattern")
  val eventFilesInputPattern = inputDir + "/" + config.getString("eventFilesInputPattern")
  val refDataFilesInputPattern = inputDir + "/" + config.getString("refDataFilesInputPattern")
  val nrScheduleFilesInputPattern = inputDir + "/" + config.getString("nrScheduleFilesInputPattern")

  // Output
  val outputDir = config.getString("outputDir")
  val schedulesOutputFile = outputDir + "/" + config.getString("schedulesOutputFile")
  val eventsOutputFile = outputDir + "/" + config.getString("eventsOutputFile")
  val tocsOutputFile = outputDir + "/" + config.getString("tocsOutputFile")
  val tplsOutputFile = outputDir + "/" + config.getString("tplsOutputFile")

  // Cassandra
  val cassandraAddr = config.getString("cassandraAddr")
  val cassandraKeyspace = config.getString("cassandraKeyspace")
  val timetablesTable = config.getString("timetablesTable")
  val tocsTable = config.getString("tocsTable")
  val tplsTable = config.getString("tplsTable")
  val routesTable = config.getString("routesTable")
  val journiesTable = config.getString("journiesTable")

}
