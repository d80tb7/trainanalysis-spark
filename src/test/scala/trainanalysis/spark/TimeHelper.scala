/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package trainanalysis.spark

import java.sql.Date
import java.sql.Timestamp

trait TimeHelper {

  def sqlDate(year: Int, month: Int, day: Int): Date = new Date(year - 1900, month - 1, day);

  def timestamp(hour: Int, min: Int): Timestamp = new Timestamp(3600000 * hour + 60000 * min)

}
