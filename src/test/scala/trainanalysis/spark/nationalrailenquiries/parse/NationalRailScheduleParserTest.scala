/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package trainanalysis.spark.nationalrailenquiries.parse

import trainanalysis.spark.UnitTest
import net.maffoo.jsonquote._
import net.maffoo.jsonquote.play._
import trainanalysis.spark.etl.nationrailenquiries.NationalRailScheduleParser
import java.time.LocalDate
import trainanalysis.spark.process.cassandra.RunDays
import trainanalysis.spark.TimeHelper
import java.time.LocalTime

/**
 * Unit Tests for the NationalRailScheduleParser class
 */
class NationalRailScheduleParserTest extends UnitTest with TimeHelper {

  "Json String containing a single route" should "be parsed" in {

    val uid = "Q21031"
    val toc = "SW"
    val stpIndicator = "P"
    val daysRun = "1000000"
    val endDate = "2016-04-21"
    val startDate = "2016-03-21"
    val tpl = "ASCOT"
    val depart = "0043"
    val trainCategory = "abcd"

    val scheduleLoc = json"[{tiploc_code: $tpl, public_departure: $depart}]"
    val scheduleSegment = json"{schedule_location: $scheduleLoc}"

    val schedule = json"{CIF_train_uid: $uid, atoc_code: $toc, CIF_stp_indicator: $stpIndicator, schedule_days_runs: $daysRun, schedule_start_date: $startDate, schedule_end_date: $endDate, schedule_segment: $scheduleSegment}"

    val scheduleWrappper = json"{JsonScheduleV1: $schedule}"

    val parsed = NationalRailScheduleParser.parse(scheduleWrappper.toString)
    parsed.length shouldEqual 1

    val parsedSchedule = parsed(0)

    parsedSchedule.uid shouldEqual uid
    parsedSchedule.toc shouldEqual toc
    parsedSchedule.stpIndicator shouldEqual stpIndicator
    parsedSchedule.startDate shouldEqual LocalDate.of(2016, 3, 21)
    parsedSchedule.endDate shouldEqual LocalDate.of(2016, 4, 21)
    parsedSchedule.runDays shouldEqual (RunDays(1 << 6))

    //parsedSchedule.scheduleSegment.trainCategory shouldEqual trainCategory

    val locs = parsedSchedule.scheduleSegment.locations
    locs.length shouldEqual 1

    val loc = locs(0)
    loc.tpl shouldEqual tpl
    loc.arrive shouldEqual(LocalTime.of(0, 43))
    loc.depart shouldEqual(LocalTime.of(0, 43))
  }

}