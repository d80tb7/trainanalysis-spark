/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */
package trainanalysis.spark

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

trait TestSparkContext {

  def withSparkContext(testCode: SparkContext => Any) {
    val sparkConf = new SparkConf().setAppName("unitTest").setMaster("local")
    val sparkContext = new SparkContext(sparkConf)
    try {
      testCode(sparkContext) // "loan" the fixture to the test
    } finally sparkContext.stop();
  }
}