/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package trainanalysis.spark.etl.networkrail.parse

import java.sql.Date
import trainanalysis.spark.UnitTest
import trainanalysis.spark.TimeHelper

class ScheduleParserTest extends UnitTest with TimeHelper{

  "A file containing a single journey" should "be parsed" in {
    val testData = <PportTimetable timetableID="20160102020723" xmlns="http://www.thalesgroup.com/rtti/XmlTimetable/v8">
                     <Journey rid="201601021305520" uid="X13022" trainId="0B00" ssd="2016-01-02" toc="SN" status="5" trainCat="BR">
                       <OR tpl="originStation" act="TB" ptd="14:28" wtd="14:28"/>
                       <IP tpl="stop1" act="T " pta="14:40" ptd="14:41" wta="14:40" wtd="14:41"/>
                       <DT tpl="destStation" act="TF" pta="15:26" wta="15:26"/>
                     </Journey>
                   </PportTimetable>

    val result = ScheduleParser.parse(testData.toString)
    val route = result.head
    result.length shouldEqual 1
    route.ssd shouldEqual sqlDate(2016, 1, 2)
    route.uid shouldEqual "X13022"
    route.toc shouldEqual "SN"
    route.isPassengerService shouldEqual false

    // Check stops in order
    val stops = route.stops
    stops.length shouldEqual 3
    stops(0).tpl shouldEqual "originStation"
    stops(0).arrive shouldEqual None
    stops(0).depart shouldEqual Some(timestamp(14, 28))

    stops(1).tpl shouldEqual "stop1"
    stops(1).arrive shouldEqual Some(timestamp(14, 40))
    stops(1).depart shouldEqual Some(timestamp(14, 41))

    stops(2).tpl shouldEqual "destStation"
    stops(2).arrive shouldEqual Some(timestamp(15, 26))
    stops(2).depart shouldEqual None
  }

  "A train marked as OL" should "be marked as a passenger service" in {
    val testData = <PportTimetable timetableID="20160102020723" xmlns="http://www.thalesgroup.com/rtti/XmlTimetable/v8">
                     <Journey rid="201601021305520" uid="X13022" ssd="2016-01-02" toc="OL" isPassengerSvc="true" trainCat="OL"/>
                     <Journey rid="201601021305521" uid="X13023" ssd="2016-01-02" toc="OL" isPassengerSvc="false" trainCat="OL"/>
                     <Journey rid="201601021305522" uid="X13024" ssd="2016-01-02" toc="OL" trainCat="OL"/>
                   </PportTimetable>
    val result = ScheduleParser.parse(testData.toString)
    result(0).isPassengerService shouldEqual true
    result(1).isPassengerService shouldEqual false
    result(2).isPassengerService shouldEqual true
  }

  "A train with operation stops" should "be be parsed" in {
    val testData = <PportTimetable timetableID="20160102020723" xmlns="http://www.thalesgroup.com/rtti/XmlTimetable/v8">
                     <Journey rid="201601021305520" uid="X13022" trainId="0B00" ssd="2016-01-02" toc="SN" status="5" trainCat="BR">
                       <OPOR tpl="originStation" act="TB" wtd="14:28"/>
                       <OPDT tpl="destStation" act="TF" wta="15:26"/>
                     </Journey>
                   </PportTimetable>
    val result = ScheduleParser.parse(testData.toString)(0)
    result.stops(0).depart shouldEqual Some(timestamp(14, 28))
    result.stops(1).arrive shouldEqual  Some(timestamp(15, 26))
  }
}
