/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package trainanalysis.spark.etl.networkrail.parse

import trainanalysis.spark.UnitTest
import trainanalysis.spark.etl.networkrail.model.TrainStopEvent
import java.sql.Date
import java.sql.Timestamp

class TrainEventParserTest extends UnitTest{

  val event1 =
    <Pport xmlns="http://www.thalesgroup.com/rtti/PushPort/v12" xmlns:ns3="http://www.thalesgroup.com/rtti/PushPort/Forecasts/v2">
      <uR>
        <TS ssd="2016-01-02" uid="C10791">
          <ns3:Location tpl="station1">
            <ns3:arr at="04:40" src="TD"/>
          </ns3:Location>
        </TS>
      </uR>
    </Pport>

  val event2 =
    <Pport xmlns="http://www.thalesgroup.com/rtti/PushPort/v12" xmlns:ns3="http://www.thalesgroup.com/rtti/PushPort/Forecasts/v2">
      <uR>
        <TS ssd="2016-01-02" uid="C10791">
          <ns3:Location tpl="station2">
            <ns3:dep at="05:40" src="TD"/>
          </ns3:Location>
        </TS>
      </uR>
    </Pport>

  "A file containing a single event with an arrival time" should "be parsed" in {
    val result = TrainEventParser.parse(event1.toString)
    result.size shouldEqual 1
    result(0).uid shouldEqual "C10791"
    result(0).ssd shouldEqual new Date(116, 0, 2)
    result(0).tpl shouldEqual "station1"
    result(0).eventTime shouldEqual new Timestamp((4 * 60 *60 + 40 * 60) * 1000 ) //04:40:00
    result(0).eventType shouldEqual TrainStopEvent.ARRIVE
  }

  "A file containing a single event with a departure time" should "be parsed" in {
    val result = TrainEventParser.parse(event2.toString)
    result.size shouldEqual 1
    result(0).uid shouldEqual "C10791"
    result(0).ssd shouldEqual new Date(116, 0, 2)
    result(0).tpl shouldEqual "station2"
    result(0).eventTime shouldEqual new Timestamp((5 * 60 *60 + 40 * 60) * 1000)  // 05:40:00
    result(0).eventType shouldEqual TrainStopEvent.DEPART
  }

  "A file containing two events" should "be parsed" in {
    val xmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
    val combinedString = xmlHeader + event1.toString + "\n" + xmlHeader + event2.toString
    val result = TrainEventParser.parse(combinedString)
    result.size shouldEqual 2
    result(0).eventType shouldEqual TrainStopEvent.ARRIVE
    result(1).eventType shouldEqual TrainStopEvent.DEPART
  }

}
