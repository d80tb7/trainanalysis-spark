/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package trainanalysis.spark.etl.networkrail.parse

import trainanalysis.spark.UnitTest

class RefDataParserTest extends UnitTest {

  "A file containing a single station without a crs" should "be parsed" in {
    val testData = <PportTimetableRef timetableId="20160102020723" xmlns="http://www.thalesgroup.com/rtti/XmlRefData/v3">
                     <LocationRef tpl="testTpl" locname="testLoc"/>
                   </PportTimetableRef>
    val result = RefDataParser.parse(testData.toString).head
    result.tpls.size shouldEqual 1
    result.tocs.size shouldEqual 0
    result.tpls.head.tpl shouldEqual "testTpl"
    result.tpls.head.locName shouldEqual "testLoc"
    result.tpls.head.crs shouldEqual None
  }

  "A file containing a single station with a crs" should "be parsed" in {
    val testData = <PportTimetableRef timetableId="20160102020723" xmlns="http://www.thalesgroup.com/rtti/XmlRefData/v3">
                     <LocationRef tpl="testTpl" locname="testLoc" crs="testCrs"/>
                   </PportTimetableRef>
    val result = RefDataParser.parse(testData.toString).head
    result.tpls.head.crs shouldEqual Some("testCrs")
  }

  "A file containing a single toc" should "be parsed" in {
    val testData = <PportTimetableRef timetableId="20160102020723" xmlns="http://www.thalesgroup.com/rtti/XmlRefData/v3">
                     <TocRef toc="testToc" tocname="testTocName" url="http://www.test.co.uk"/>
                   </PportTimetableRef>
    val result = RefDataParser.parse(testData.toString).head
    result.tpls.size shouldEqual 0
    result.tocs.size shouldEqual 1
    result.tocs.head.code shouldEqual "testToc"
    result.tocs.head.name shouldEqual "testTocName"
  }

  "A file containing both a station and a toc" should "be parsed" in {
    val testData = <PportTimetableRef timetableId="20160102020723" xmlns="http://www.thalesgroup.com/rtti/XmlRefData/v3">
                     <LocationRef tpl="testTpl" locname="testLoc" crs="testCrs"/>
                     <TocRef toc="testToc" tocname="testTocName" url="http://www.test.co.uk"/>
                   </PportTimetableRef>
    val result = RefDataParser.parse(testData.toString).head

    //tocs
    result.tocs.size shouldEqual 1
    result.tocs.head.code shouldEqual "testToc"
    result.tocs.head.name shouldEqual "testTocName"

    //tpl
    result.tpls.size shouldEqual 1
    result.tpls.head.tpl shouldEqual "testTpl"
    result.tpls.head.locName shouldEqual "testLoc"
    result.tpls.head.crs shouldEqual Some("testCrs")
  }
}
