/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package trainanalysis.spark.etl.nationalrailenquiries

import trainanalysis.spark.UnitTest
import trainanalysis.spark.TestSparkContext
import trainanalysis.spark.TimeHelper
import trainanalysis.spark.etl.nationrailenquiries.ScheduleLocation
import trainanalysis.spark.etl.nationrailenquiries.ScheduleSegment
import trainanalysis.spark.process.cassandra.RunDays
import java.time.LocalDate
import java.time.LocalTime
import trainanalysis.spark.etl.nationrailenquiries.Schedule
import trainanalysis.spark.etl.nationrailenquiries.NationalRailScheduleProcessor

class NationalRailConsolodationTest extends UnitTest with TestSparkContext {

  "Overlay Schedules" should "be filtered out" in withSparkContext { (sparkContext) =>

    val stop = ScheduleLocation("station1", "LO", None, Some(LocalTime.of(12, 0)))
    val segment = ScheduleSegment("OO", Seq(stop))
    val schedule = Schedule("uid1", "toc1", LocalDate.of(2016, 1, 1), LocalDate.of(2016, 2, 1), RunDays(2), "O", segment)

    val scheduleRdd = sparkContext.parallelize(Seq(schedule))

    val results = NationalRailScheduleProcessor.consolodate(scheduleRdd).collect()

    results.length shouldEqual 0

  }

  "Permanent Schedules" should "be retained" in withSparkContext { (sparkContext) =>

    val stop = ScheduleLocation("station1", "LO", None, Some(LocalTime.of(12, 0)))
    val segment = ScheduleSegment("OO", Seq(stop))
    val schedule = Schedule("uid1", "toc1", LocalDate.of(2016, 1, 1), LocalDate.of(2016, 2, 1), RunDays(2), "P", segment)

    val scheduleRdd = sparkContext.parallelize(Seq(schedule))

    val results = NationalRailScheduleProcessor.consolodate(scheduleRdd).collect()

    results.length shouldEqual 1

  }

  "Duplicate Schedules" should "be merged" in withSparkContext { (sparkContext) =>

    val stop = ScheduleLocation("station1", "LO", None, Some(LocalTime.of(12, 0)))
    val segment = ScheduleSegment("OO", Seq(stop))
    val schedule1 = Schedule("uid1", "toc1", LocalDate.of(2016, 1, 1), LocalDate.of(2016, 2, 1), RunDays(2), "P", segment)
    val schedule2 = Schedule("uid1", "toc1", LocalDate.of(2016, 1, 10), LocalDate.of(2016, 3, 1), RunDays(4), "P", segment)

    val scheduleRdd = sparkContext.parallelize(Seq(schedule1, schedule2))

    val results = NationalRailScheduleProcessor.consolodate(scheduleRdd).collect()

    results.length shouldEqual 1

    val returnedSched = results(0)

    returnedSched.uid shouldEqual "uid1"
    returnedSched.toc shouldEqual "toc1"
    returnedSched.runDays shouldEqual (RunDays(4 | 2))
    returnedSched.startDate shouldEqual (schedule1.startDate)
    returnedSched.endDate shouldEqual (schedule2.endDate)
    returnedSched.scheduleSegment shouldEqual segment
  }

  "Schedules with the same uid but different stops" should "be not be merged" in withSparkContext { (sparkContext) =>

    val stop1 = ScheduleLocation("station1", "LO", None, Some(LocalTime.of(12, 0)))
    val stop2 = ScheduleLocation("station2", "LO", None, Some(LocalTime.of(13, 0)))
    val segment1 = ScheduleSegment("OO", Seq(stop1))
    val segment2 = ScheduleSegment("OO", Seq(stop1, stop2))
    val schedule1 = Schedule("uid1", "toc1", LocalDate.of(2016, 1, 1), LocalDate.of(2016, 2, 1), RunDays(2), "P", segment1)
    val schedule2 = Schedule("uid1", "toc1", LocalDate.of(2016, 1, 10), LocalDate.of(2016, 3, 1), RunDays(4), "P", segment2)

    val scheduleRdd = sparkContext.parallelize(Seq(schedule1, schedule2))

    val results = NationalRailScheduleProcessor.consolodate(scheduleRdd).collect()

    results.length shouldEqual 2
  }

  "Schedules with the same stops but a different uid" should "not be merged" in withSparkContext { (sparkContext) =>

    val stop = ScheduleLocation("station1", "LO", None, Some(LocalTime.of(12, 0)))
    val segment = ScheduleSegment("OO", Seq(stop))
    val schedule1 = Schedule("uid1", "toc1", LocalDate.of(2016, 1, 1), LocalDate.of(2016, 2, 1), RunDays(2), "P", segment)
    val schedule2 = Schedule("uid2", "toc1", LocalDate.of(2016, 1, 10), LocalDate.of(2016, 3, 1), RunDays(4), "P", segment)

    val scheduleRdd = sparkContext.parallelize(Seq(schedule1, schedule2))

    val results = NationalRailScheduleProcessor.consolodate(scheduleRdd).collect()

    results.length shouldEqual 2

  }

}