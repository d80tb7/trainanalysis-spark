/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package trainanalysis.spark.etl.nationalrailenquiries

import trainanalysis.spark.UnitTest
import java.time.LocalTime
import trainanalysis.spark.etl.nationrailenquiries.ScheduleLocation
import trainanalysis.spark.etl.nationrailenquiries.ScheduleSegment
import java.time.LocalDate
import trainanalysis.spark.process.cassandra.RunDays
import trainanalysis.spark.etl.nationrailenquiries.Schedule
import trainanalysis.spark.etl.nationrailenquiries.NationalRailScheduleProcessor
import trainanalysis.spark.TimeHelper
import trainanalysis.spark.TestSparkContext
import trainanalysis.spark.etl.networkrail.model.Tpl

class NationalRailScheduleProcessorTest extends UnitTest with TestSparkContext with TimeHelper {

  "An RDD of of schedules" should "be converted to Routes and Journies" in withSparkContext { (sparkContext) =>

    // Schedule with three stops
    val stop1 = ScheduleLocation("station1", "LO", None, Some(LocalTime.of(12, 0)))
    val stop2 = ScheduleLocation("station2", "LP", Some(LocalTime.of(13, 0)), Some(LocalTime.of(13, 1)))
    val stop3 = ScheduleLocation("station3", "LT", Some(LocalTime.of(14, 0)), None)
    val segment = ScheduleSegment("OO", Seq(stop1, stop2, stop3))
    val schedule = Schedule("uid1", "toc1", LocalDate.of(2016, 1, 1), LocalDate.of(2016, 2, 1), RunDays(2), "C", segment)

    val station1 = Tpl("station1", Some("station1Crs"), Some("SWT"), "");
    val station2 = Tpl("station2", Some("station2Crs"), Some("SWT"), "");
    val station3 = Tpl("station3", Some("station3Crs"), Some("SWT"), "");

    val scheduleRdd = sparkContext.parallelize(Seq(schedule))
    val stationsRdd = sparkContext.parallelize(Seq(station1, station2, station3))

    val results = NationalRailScheduleProcessor.toRoutesAndJournies(sparkContext, scheduleRdd, stationsRdd).collect

    results.length shouldEqual 1

    // Route
    val route = results(0)._1
    route.uid shouldEqual "uid1"
    route.startCrs shouldEqual "station1Crs"
    route.endCrs shouldEqual "station3Crs"
    route.validFrom shouldEqual sqlDate(2016, 1, 1)
    route.validTo shouldEqual sqlDate(2016, 2, 1)
    route.runDays shouldEqual RunDays(2)
    route.startTime shouldEqual timestamp(12, 0)
    route.endTime shouldEqual timestamp(14, 0)
    route.toc shouldEqual "toc1"

    // Journies
    val journies = results(0)._2

    journies.length shouldEqual 3

    val journey1 = journies(0)
    journey1.route shouldEqual route
    journey1.startCrs shouldEqual "station1Crs"
    journey1.endCrs shouldEqual "station2Crs"
    journey1.startTime shouldEqual timestamp(12, 0)
    journey1.endTime shouldEqual timestamp(13, 0)

    val journey2 = journies(1)
    journey2.route shouldEqual route
    journey2.startCrs shouldEqual "station1Crs"
    journey2.endCrs shouldEqual "station3Crs"
    journey2.startTime shouldEqual timestamp(12, 0)
    journey2.endTime shouldEqual timestamp(14, 0)

    val journey3 = journies(2)
    journey3.route shouldEqual route
    journey3.startCrs shouldEqual "station2Crs"
    journey3.endCrs shouldEqual "station3Crs"
    journey3.startTime shouldEqual timestamp(13, 1)
    journey3.endTime shouldEqual timestamp(14, 0)
  }
}