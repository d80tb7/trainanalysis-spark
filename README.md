
Trainanalysis-Spark
========================

Spark Jobs for Train Aalysis.  Baeed on [Train Analysis Spark Docker](https://bitbucket.org/d80tb7/trainanalysis-docker-spark) so it can be distributed as a self-submitting docker container. 
  Currently the Jobs supported are:

- trainanalysis.spark.ImportNetworkRailFiles: ETL job which takes all Network Rail XML files and loads onto HDFS in parquet format for later processing.
- trainanalysis.spark.ProcessNetworkRailData: Takes the Network Rail Parquet files produced by ETL (Tocs, Tpls etc) and processes them into the various Cassandra tables required by the webapi.

The docker image contains a fat jar (located under /app/scratch-assembly-dev.jar) along with spark-submit.sh (located under /spark/spark-submit.sh).





