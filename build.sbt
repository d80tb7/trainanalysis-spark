import trainanlysis.sbt.Dependencies._

name := "trainanalysis-spark"

// Dependencies
libraryDependencies ++= Seq( sparkCore, 
		             sparkSql,  
			     scalaLogging,
       		             logback,
                             cassandraConnector,
                             woodstox,
                             jacksonCore,
                             jacksonScala,
                             jacksonAnnotations,
                             "com.fasterxml.jackson.datatype" % "jackson-datatype-jsr310" % "2.7.5",
                             jacksonJdk8Types,
                             "org.cmj" % "trainanalysis-networkrail-model_2.11" % "2", 
                             scalatest,
                             scalamock,
			    "net.maffoo" %% "jsonquote-core" % "0.4.0" % "test",
                            "net.maffoo" %% "jsonquote-play" % "0.4.0" % "test" )

// For some reason this isn't being brought in by the plugin
val scalaStyleConfigLoc = "https://bitbucket.org/d80tb7/trainanalysis-sbt-plugin/raw/master/src/main/resources/scalastyle-config.xml"
scalastyleConfigUrl := Some(url(scalaStyleConfigLoc))

//EclipseKeys.withSource := true

// Merge Strategy for creating fat jar
assemblyMergeStrategy in assembly := {
  case x if x.endsWith(".class") => MergeStrategy.last
  case x if x.endsWith(".properties") => MergeStrategy.last
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    if (oldStrategy == MergeStrategy.deduplicate) MergeStrategy.first else oldStrategy(x)
}

// Docker
enablePlugins(DockerPlugin)
dockerfile in docker := {
  val artifact: File = assembly.value
  val artifactTargetPath = s"/app/${name.value}.jar"

  new Dockerfile {
    from("d80tb7/trainanalysis-spark-docker-base")
    add(artifact, artifactTargetPath)
  }
}

imageNames in docker := Seq(
  ImageName(s"d80tb7/${name.value}:latest"),
  ImageName(s"d80tb7/${name.value}:${version.value}")
)

