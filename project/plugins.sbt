addSbtPlugin("se.marcuslonnberg" % "sbt-docker" % "1.4.0")
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.3")
addSbtPlugin("org.cmj" % "trainanalysis-sbt-plugin" % "24")
addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.8.2")
